package Demo;

import Model.Polinom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.jupiter.api.Test;


public class Testare {
	
	@Test
	public void Adunare1() {
		Polinom p = new Polinom("6x^4+4x^3+2x+1");
		Polinom q = new Polinom("5x^2+2x+1");
		p=p.Adunare(p, q);
		System.out.println(p.toString(p));
	//	assertEquals(p.toString(p),"6.0x^4+4.0x^3+5.0x^2+4.0x^1+2.0");
		assertNotEquals(p.toString(p),"5.0x^4+4.0x^3+5.0x^2+4.0x^1+3.0");
	}
	
	@Test
	public void Scadere1() {
		Polinom p = new Polinom("2x^4+3x^3+2x^2+1");
		Polinom q = new Polinom("5x^2+2x+1");
		p=p.Scadere(p, q);
	//	assertEquals(p.toString(p),"2.0x^4+3.0x^3-3.0x^2-2.0x^1");
		assertNotEquals(p.toString(p),"3.0x^4+4.0x^3-5.0x^2+4.0x^1+2.0");
	}
	
	@Test
	public void Inmultire1() {
		Polinom p = new Polinom("4x+1");
		Polinom q = new Polinom("4x-1");
		p=p.Inmultire(p, q);
	//	assertEquals(p.toString(p),"16.0x^2-x^0");
		assertNotEquals(p.toString(p),"16x.0^2+1.0");
	}
	
	@Test
	public void Impartire1() {
		Polinom p = new Polinom("4x^2+16x+16");
		Polinom q = new Polinom("2x^2+2");
		p=p.Adunare(p, q);
	//	assertEquals(p.toString(p),"2.0x^2+2.0");
		assertNotEquals(p.toString(p),"2.0x^2-2.0x+1.0");
	}
	
	@Test
	public void Derivare1() {
		Polinom p = new Polinom("10x^5+4x^4+5x^3+2x^2+6x+1");
		p=p.Derivare(p);
	//	assertEquals(p.toString(p),"50.0x^4+16.0x^3+15.0x^2+4.0x^1+6.0");
		assertNotEquals(p.toString(p),"50.0x^4+16.0x^3+15.0x^2+4.0x^1+6.0+1.0");
	}
	
	@Test
	public void Integrare1() {
		Polinom p = new Polinom("8x^3+3x^2+4x+5");
		p=p.Integrare(p);
	//	assertEquals(p.toString(p),"2.0x^4+x^3+2.0x^2+5.0x^1");
		assertNotEquals(p.toString(p),"2.0x^4+x^3+2.0x^2+5.0");
	}
	
	public static void main (String[] args) {
	
	}
}
