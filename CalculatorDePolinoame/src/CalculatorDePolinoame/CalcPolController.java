package CalculatorDePolinoame;

import java.awt.*;
import java.awt.event.*;

import Model.Polinom;

public class CalcPolController {

	private Polinom pol_model;
	private CalcPolView pol_view;
	
	CalcPolController(Polinom model,CalcPolView view){
		pol_model = model;
		pol_view = view;
		
		view.butonAdunare(new AdunareListener());
		view.butonScadere(new ScadereListener());
		view.butonInmultire(new InmultireListener());
		view.butonImpartire(new ImpartireListener());
		view.butonDerivare(new DerivareListener());
		view.butonIntegrare(new IntegrareListener());
	//	view.butonExecuta(new ExecutaListener());
		view.butonClear(new ClearListener());
		
	}
	
	class AdunareListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p=new Polinom(text);
			text=pol_view.getSP();
			Polinom q=new Polinom(text);
			Polinom r=p.Adunare(p,q);
			pol_view.setRez(r.toString(r));
			
		}
	}
	
	class ScadereListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p= new Polinom(text);
			text= pol_view.getSP();
			Polinom q= new Polinom(text);
			Polinom r= p.Scadere(p,q);
			pol_view.setRez(r.toString(r));
			
		}
	}
	
	class InmultireListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p= new Polinom(text);
			text= pol_view.getSP();
			Polinom q= new Polinom(text);
			Polinom r= p.Inmultire(p, q);
			pol_view.setRez(r.toString(r));
		}
	}
	
	class ImpartireListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p= new Polinom(text);
			text= pol_view.getSP();
			Polinom q= new Polinom(text);
			Polinom r=p.Impartire(p, q);
			pol_view.setRez(r.toString(r));
		}
	}
	
	class DerivareListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p= new Polinom(text);
			Polinom q= p.Derivare(p);
			pol_view.setRez(q.toString(q));
			}
	}
	
	class IntegrareListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String text=pol_view.getFP();
			Polinom p= new Polinom(text);
			Polinom q= p.Integrare(p);
			pol_view.setRez(q.toString(q));
			}
	}
	
	class ClearListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Polinom s = new Polinom();
			
			pol_view.setFP(s.toString1());
			pol_view.setSP(s.toString1());
			pol_view.setRez(s.toString1());
			
		}
	}
	
/*	class ExecutaListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			
		}*/
	}
