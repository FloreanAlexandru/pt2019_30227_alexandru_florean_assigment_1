package CalculatorDePolinoame;

import java.awt.*;
import javax.swing.*;
import javax.swing.text.JTextComponent;

import Model.Polinom;

import java.awt.event.ActionListener;

public class CalcPolView  extends JFrame{
	
	private JLabel eticheta1=new JLabel("Primul polinom");
	private JTextField first= new JTextField(35);
	private JLabel eticheta2=new JLabel("Al doilea polinom");
	private JTextField second= new JTextField(35);
	private JLabel eticheta3= new JLabel("Rezultat");
	private JTextField third= new JTextField(35);
	
	private JButton buton1=new JButton("Adunare");
	private JButton buton2=new JButton("Scadere");
	private JButton buton3=new JButton("Inmultire");
	private JButton buton4=new JButton("Impartire");
	private JButton buton5=new JButton("Derivare");
	private JButton buton6=new JButton("Integrare");
	//private JButton buton7=new JButton("Executa");
	private JButton buton8=new JButton("Clear");
	private Polinom model_m;
	
public CalcPolView(Polinom model) {
	
	JFrame frame= new JFrame("Calculator de polinoame");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(400,400);
	JPanel c1=new JPanel();
	JPanel c2=new JPanel();
	JPanel c3=new JPanel();
	JPanel c4=new JPanel();
	JPanel c5=new JPanel();
	JPanel c6=new JPanel();

	c1.add(eticheta1);
	c1.add(first);
	c1.setLayout(new FlowLayout());
	
	c2.add(eticheta2);
	c2.add(second);
	c2.setLayout(new FlowLayout());
	
	c3.add(eticheta3);
	c3.add(third);
	c3.setLayout(new FlowLayout());
	
	c4.add(c1);
	c4.add(c2);
	c4.add(c5);
	c4.setLayout(new BoxLayout(c4,BoxLayout.Y_AXIS));

	c5.add(buton1);
	c5.add(buton2);
	c5.add(buton3);
	c5.add(buton4);
	c5.add(buton5);
	c5.add(buton6);
	//c5.add(buton7);
	c5.add(buton8);
	c5.setLayout(new FlowLayout());
	
	c6.add(c4);
	c6.add(c3);
	c6.setLayout(new BoxLayout(c6,BoxLayout.Y_AXIS));
	
	frame.setContentPane(c6);
	frame.setVisible(true);
	
}

public void butonAdunare(ActionListener a) {
	buton1.addActionListener(a);
}

public void butonScadere(ActionListener b) {
	buton2.addActionListener(b);
}

public void butonInmultire(ActionListener a) {
	buton3.addActionListener(a);
}

public void butonImpartire(ActionListener a) {
	buton4.addActionListener(a);
}

public void butonDerivare(ActionListener a) {
	buton5.addActionListener(a);
}

public void butonIntegrare(ActionListener a) {
	buton6.addActionListener(a);
}

public void butonClear(ActionListener a) {
	buton8.addActionListener(a);
}

/*
public void butonExecuta(ActionListener a) {
	buton7.addActionListener(a);
}*/

public void butonClear( ) {
	buton8.setText("");
}

public String getFP() {
	return first.getText();
}

public String getSP() {
	return second.getText();
}

public void setFP(String a) {
	first.setText(a);
}

public void setSP(String a) {
	second.setText(a);
}

public void setRez(String a) {
	 third.setText(a);
}

}