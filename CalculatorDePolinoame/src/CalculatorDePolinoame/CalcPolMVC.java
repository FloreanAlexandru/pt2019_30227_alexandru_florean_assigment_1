package CalculatorDePolinoame;

import java.awt.*;
import javax.swing.*;

import Model.Polinom;

import java.awt.event.ActionListener;

public class CalcPolMVC {

	public static void main(String[] args) {

		Polinom model = new Polinom();
		CalcPolView view =new CalcPolView(model);
		CalcPolController controller = new CalcPolController(model,view);
		
		view.setVisible(true);
	}
}
