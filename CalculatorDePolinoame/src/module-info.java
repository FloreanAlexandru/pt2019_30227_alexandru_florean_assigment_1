/**
 * 
 */
/**
 * @author 40751
 *
 */
module CalculatorDePolinoame {
	requires java.desktop;
	requires junit;
	requires org.junit.jupiter.api;
	requires org.junit.jupiter.params;
	requires org.junit.jupiter.engine;
	requires org.junit.jupiter.migrationsupport;
	requires org.junit.vintage.engine;
	requires org.junit.platform.commons;
	requires org.junit.platform.engine;
	requires org.junit.platform.runner;
	requires org.junit.platform.launcher;
	requires org.junit.platform.suite.api;
}