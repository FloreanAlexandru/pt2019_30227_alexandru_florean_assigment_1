package Model;

public class Monom implements Comparable <Monom>{
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + power;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monom other = (Monom) obj;
		if (power != other.power)
			return false;
		return true;
	}

	private int power;
	private double coef;
	
	public Monom(int power, double coef) {
		super();
		this.power = power;
		this.coef = coef;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	@Override
	public int compareTo(Monom o) {
			if(((Monom)o).getPower() > this.getPower()){
				return 1;
				}
			else if(((Monom)o).getPower() < this.getPower()) {
					return -1;
				}
				else 
					return 0;
			}
}	


