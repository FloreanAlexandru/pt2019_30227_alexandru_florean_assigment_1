package Model;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom {
	private ArrayList<Monom> Lista = new ArrayList<Monom>();
	
	public Polinom() {
		Monom a=new Monom(0,0);
		Lista.add(a);
	}
	
	public Polinom(String polinom) {
		this.Parsare(polinom);
	}
	
	public String toString1( ) {
		String a="";
		return a;
	}
	
	public String toString(Polinom p) {
		String s="";
		for(Monom i:p.Lista) {
		if(i.getCoef()!=0) {
			if(i.getCoef()<0) {
				s=s+" "+i.getCoef();
			}
			else if(i.getCoef()>0 && s==""){
				s=s+" "+i.getCoef();
			}
			else if(i.getCoef()==1 && i.getPower()==0) {
				s=s+"+1";
			}
			else {
				s=s+"+"+i.getCoef();
			}
			//System.out.println(s);
			if(s.contentEquals(" 1.0")) {
				s="";
			}else if(s.contentEquals(" -1.0")) {
				s="-";
			}
			String s1="";
			s1=s1+"x^"+i.getPower();
			
			if(s1.contentEquals("x^0") && i.getCoef()>=1) {
				s1="";
			}
			s=s+s1;
			}
		}
		s=s.replaceAll("x^1","x").replaceAll("1.0x","x").replaceAll("x^0", "1");
		return s;
	}
	
	private void Parsare(String pol) {
		String pol1= pol.replaceAll("-","+-");
		String[] part= pol1.split("\\+");
		double a;
		int b;
		for(String i: part) {
			if(i.length()!=0) {
				a=b=0;
				String[] impart1 = i.split("x");
				System.out.println(i);	 
				System.out.println("Prima impartire");
				for(String j: impart1) {
					System.out.println(j);
					}
				if(impart1.length == 0) {
					a=1.0;
					b=1;
					}
				else if(impart1.length == 1) {
					if(i.charAt(i.length()-1) == 'x') {
						b=1;
						}
					else {
						b=0;
						}
					if(impart1[0].charAt(0)== '-') {
						if(impart1[0].length()>1) {
							a= Double.parseDouble(impart1[0]);
							}
						else {
							a=-1;
							}
						}
					else {
						a=Double.parseDouble(impart1[0]);
						}
					}	
				else if(impart1[0].length()==0 && impart1[1].length()<2) {
					a=1;
					}
				else if(impart1[0].length()==1 && impart1[0].contains("-")) {
					a=-1;
					if(impart1[1].contains("^")) {
						String[] impart2=impart1[1].split("\\^");
						b=Integer.parseInt(impart2[1]);
						}
					}
				else if(impart1.length > 1) {
					if(impart1[0].contains("") && impart1[0].length()==0) {
					 a=1;}
					else {
				a=Double.parseDouble(impart1[0]);
				}
					if(impart1[1].contains("^")) {
				
						String[] impart2=impart1[1].split("\\^");
					
						b=Integer.parseInt(impart2[1]);
					}
				
				}
				else if(impart1[0].length()==0 && impart1[1].length()==2) {
					String[] impart2=impart1[1].split("\\^");
					b=Integer.parseInt(impart2[1]);
					}
				
				if(a==0){
					b=0;}
				int ok=0;
			for(Monom k: Lista) {
				if(k.getPower()==b) {
					if(k.getCoef()+a!=0) {
						k.setCoef(k.getCoef()+a);
					}else 
					{
						Lista.remove(k);
					}
					ok=1;
					break;
				}
			}	
			if(ok!=1){
			Monom x=new Monom(b,a);
			Lista.add(x);
			}
		}
	}
}	

	public void Sortare(Polinom p) {
			Collections.sort(p.Lista);
		}

	
	public Polinom Adunare(Polinom p,Polinom q) {
		Polinom r=new Polinom();
		int ok=0;
		 for(Monom i: p.Lista) {
			 for(Monom j: q.Lista) {
				 if(i.getPower()==j.getPower()) {
					i.setCoef(i.getCoef()+j.getCoef());
				 }
			 }
			 r.Lista.add(i);
		 }
		 for(Monom i: q.Lista) {
			 ok=0;
			 for(Monom j: p.Lista) {
				 if(i.getPower()==j.getPower())
					 ok=1;
			 }
			 if(ok==0)
				 r.Lista.add(i);
		 }
		 Sortare(r);
		 return r;
	}

	public Polinom Scadere(Polinom p,Polinom q) {
		Polinom r=new Polinom();
		int ok=0;
			for(Monom i: p.Lista) {
				for(Monom j: q.Lista) {
					if(i.getPower()==j.getPower()) {
						i.setCoef(i.getCoef()-j.getCoef());
					}
				}
				r.Lista.add(i);
			}
			 for(Monom i: q.Lista) {
				 ok=0;
				 for(Monom j: p.Lista) {
					 if(i.getPower()==j.getPower())
						 ok=1;
				 }
				 if(ok==0) {
					 i.setCoef(i.getCoef()*(-1));
					 r.Lista.add(i);
			 }
			}
			 Sortare(r);
			 return r;
	}
	
	public Polinom Inmultire(Polinom p,Polinom q) {
		Polinom r=new Polinom();
		Polinom r1=new Polinom();
		Polinom r2=new Polinom(); r2.Lista.remove(0);
		int []v1=new int [1000];double []v2= new double [1000];
		Sortare(p); Sortare(q);
		for(Monom i: p.Lista) {
			for(Monom j:q.Lista) {
				Monom a=new Monom(0,0);
				a.setCoef(i.getCoef()*j.getCoef());
				a.setPower(i.getPower()+j.getPower());
				r.Lista.add(a);
				r1.Lista.add(a);
		}
	}
		Sortare(r1); r.Lista.clear();
		r2=r1;
		for(int i=0;i<r1.Lista.size();i++) {
			v1[i]=r1.Lista.get(i).getPower();
			v2[i]=r1.Lista.get(i).getCoef();
			for(int j=i+1;j<r2.Lista.size();j++) {
				if(r1.Lista.get(i).getPower()==r2.Lista.get(j).getPower()) {
					v2[i]=v2[i]+r2.Lista.get(j).getCoef();
					r2.Lista.get(j).setCoef(0);
					r2.Lista.get(j).setPower(0);
				}
			}
			r.Lista.add(new Monom(v1[i],v2[i]));
}		Sortare(r);
		return r;
}
	
	public Polinom Impartire(Polinom p,Polinom q) {
		Sortare(p); Sortare(q);
		Polinom r= p;
		Polinom r1=q;
		Polinom r2=new Polinom();
		Polinom r3=new Polinom();
		Polinom r4=new Polinom();
		r2.Lista.remove(0);r3.Lista.remove(0);r4.Lista.remove(0);
		
		System.out.println("Restul:" +r.toString(r));
		System.out.println("Catul:"+r4.toString(r4));
		
		if(p.Lista.get(0).getPower()<q.Lista.get(0).getPower()) {
			return p;
		}
		
		
		while( r.Lista.size()>0 && r.Lista.get(0).getPower() >= q.Lista.get(0).getPower()) {
			Monom a= new Monom(r.Lista.get(0).getPower()-q.Lista.get(0).getPower(),r.Lista.get(0).getCoef()/q.Lista.get(0).getCoef());
			r2.Lista.add(a);
			r3=r3.Inmultire(r2,r1);
			r=r.Scadere(r, r3);
			r2.Lista.remove(0);
			r4.Lista.add(a);
			r.Lista.remove(0);
			Sortare(r4);
			
		}	
			Sortare(r4);
			return r4;
}
	
	public Polinom Derivare(Polinom p) {
		Polinom r = new Polinom();
		
		for(Monom i: p.Lista) {
			Monom a=new Monom(0,0);
			if(i.getPower()==1 && i.getCoef()==1) {
				a.setCoef(1);
				a.setPower(0);
				r.Lista.add(a);
			}
			if(i.getPower()>0 && i.getCoef()!=0) {
				a.setCoef(i.getCoef()*i.getPower());
				a.setPower(i.getPower()-1);
				r.Lista.add(a);
			}
			else if(i.getCoef()!=0 && i.getPower()==1) {
				a.setCoef(i.getCoef());
				a.setPower(0);
				r.Lista.add(a);
			}
		}
		Sortare(r);
		return r;
	}
	
	public Polinom Integrare(Polinom p) {
		Polinom r=new Polinom();
		
		for(Monom i: p.Lista) {
			Monom a=new Monom(0,0);
			if(i.getCoef()!=0 && i.getPower()==0) {
				a.setCoef(i.getCoef());
				a.setPower(1);
				r.Lista.add(a);
			}
			else if(i.getPower()>0) {
				a.setCoef(i.getCoef()/(i.getPower()+1));
				a.setPower(i.getPower()+1);
				r.Lista.add(a);
			}
		}
		Sortare(r);
		return r;
	}
	

}
	